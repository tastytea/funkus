SVGS = svg/*.svg svg/animated/*
SIZES = 512 256 128

images: funkus/webp funkus/png

release: images
	tar -caf funkus-images.tar.zst funkus/

funkus/webp: $(SVGS)
	rm -rf webp funkus/webp
	$(foreach size,$(SIZES), \
		mkdir -p funkus/webp && \
		./generate_raster_images.zsh webp $(size) && \
		mv webp funkus/webp/$(size) ;)

funkus/png: $(SVGS)
	rm -rf png funkus/png
	$(foreach size,$(SIZES), \
		mkdir -p funkus/png && \
		./generate_raster_images.zsh png $(size) && \
		mv png funkus/png/$(size) ;)

clean:
	rm -rf funkus webp png
	rm -f funkus-images.tar.zst
